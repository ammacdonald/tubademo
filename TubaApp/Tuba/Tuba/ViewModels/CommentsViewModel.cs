﻿using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tuba.Models;
using Tuba.Parameters;
using Tuba.Services;

namespace Tuba.ViewModels
{
    public class CommentsViewModel : MvxViewModel
    {
        private News news;
        private NewsParameters newsParameters;
        private INewsService newsService;
        public CommentsViewModel(INewsService newsService)
        {
            this.newsService = newsService;
        }

        public News News
        {
            get
            {
                return news;
            }

            set
            {
                news = value;
                RaisePropertyChanged(() => News);
            }
        }

        public void Init(NewsParameters newsParameters)
        {
            this.newsParameters = newsParameters;
        }

        public override async void Start()
        {
            this.News = await this.newsService.GetNews(this.newsParameters.NewsItemId);
        }
    }
}
