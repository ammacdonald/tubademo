using Acr.UserDialogs;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using Tuba.Models;
using Tuba.Parameters;
using Tuba.Services;

namespace Tuba.ViewModels
{
    public class FirstViewModel
        : MvxViewModel
    {
        private INewsService newsService;
        private ObservableCollection<News> items;
        private MvxCommand nextPageCommand;
        private int page;
        private IUserDialogs userDialogs;

        public ObservableCollection<News> Items
        {
            get { return items; }
            set
            {
                items = value;
                RaisePropertyChanged(() => Items);
            }
        }

        public MvxCommand NextPageCommand
        {
            get
            {
                return new MvxCommand(async () => {
                    page++;
                    var nextPageItems = await this.newsService.GetNews(page, 10);
                    foreach(var item in nextPageItems)
                    {
                        item.PropertyChanged += (object sender, PropertyChangedEventArgs e) =>
                        {
                            if (e.PropertyName.Equals("ShowCommentsCommand"))
                                ShowViewModel<CommentsViewModel>(new NewsParameters() { NewsItemId = item.Id });
                        };
                        this.Items.Add(item);
                    }
                });
            }
        }

        public FirstViewModel(INewsService newsService, IUserDialogs userDialogs)
        {
            this.newsService = newsService;
            this.userDialogs = userDialogs;
        }

        public override async void Start()
        {
            var result = await this.userDialogs.PromptAsync(new PromptConfig()
                            .SetTitle("Enter service ip address.")
                            .SetPlaceholder("192.168.2.11")
                            .SetMaxLength(39));
            NewsService.ip = result.Text;
            var news = await this.newsService.GetNews(page, 10);
            foreach(var item in news)
            {
                item.PropertyChanged += (object sender, PropertyChangedEventArgs e) =>
                {
                    if (e.PropertyName.Equals("ShowCommentsCommand"))
                        ShowViewModel<CommentsViewModel>(new NewsParameters() { NewsItemId = item.Id });
                };
            }
            this.Items = new ObservableCollection<News>(news);
        }
    }
}
