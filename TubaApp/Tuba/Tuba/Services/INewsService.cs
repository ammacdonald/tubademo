﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tuba.Models;

namespace Tuba.Services
{
    public interface INewsService
    {
        Task<List<News>> GetNews(int page, int numberOfItems);
        Task<News> GetNews(int newsItemId);
    }
}
