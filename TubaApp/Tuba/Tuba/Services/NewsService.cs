﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Tuba.Models;

namespace Tuba.Services
{
    public class NewsService : INewsService
    {
        private HttpClient client;
        private const string baseAddress = "http://{0}:5000/";
        public static string ip = "";
        public NewsService(HttpClient client)
        {
            this.client = client;
        }
        public async Task<List<News>> GetNews(int page, int numberOfItems)
        {
            var address = string.Format(baseAddress, ip);
            List<News> news = null;
            client.BaseAddress = new Uri(string.Format(baseAddress, ip));
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = await client.GetAsync(string.Format("api/news?page={0}&numberOfItems={1}", page, numberOfItems));
            if (response.IsSuccessStatusCode)
            {
                news = await response.Content.ReadAsAsync<List<News>>();
            }
            return news;
        }

        public async Task<News> GetNews(int newsItemId)
        {
            News news = null;
            client.BaseAddress = new Uri(string.Format(baseAddress, ip));
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = await client.GetAsync(string.Format("api/news/{0}", newsItemId));
            if (response.IsSuccessStatusCode)
            {
                news = await response.Content.ReadAsAsync<News>();
            }
            return news;
        }
    }
}
