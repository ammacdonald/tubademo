﻿using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Linq.Expressions;

namespace Tuba.Models
{
    public class News : INotifyPropertyChanged
    {
        private bool areCommentsVisible;

        public DateTime PublishTime { get; set; }
        public User Actor { get; set; }
        public User Patient { get; set; }
        public string Body { get; set; }
        public List<Comment> Comments { get; set; }
        public int Id { get; set; }
        public int Shares { get; set; }
        public string Action { get; set; }

        public MvxCommand ExpandCommentCommand
        {
            get
            {
                return new MvxCommand(() => {
                    this.AreCommentsVisible = !this.AreCommentsVisible;
                });
            }
        }

        public MvxCommand ShowCommentsCommand
        {
            get
            {
                return new MvxCommand(() => {
                    PropertyChanged(this, new PropertyChangedEventArgs("ShowCommentsCommand"));
                });
            }
        }

        public bool AreCommentsVisible
        {
            get
            {
                return areCommentsVisible;
            }

            set
            {
                areCommentsVisible = value;
                PropertyChanged(this, new PropertyChangedEventArgs("AreCommentsVisible"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
