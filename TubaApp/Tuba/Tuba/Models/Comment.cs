﻿using MvvmCross.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tuba.Models
{
    public class Comment
    {
        public User Poster { get; set; }
        public string Body { get; set; }
    }
}
