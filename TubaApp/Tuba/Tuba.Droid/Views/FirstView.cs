using Acr.UserDialogs;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Util;
using Android.Widget;
using MvvmCross.Binding.Droid.Views;
using MvvmCross.Droid.Support.V7.RecyclerView;
using MvvmCross.Droid.Views;
using MvvmCross.Platform;
using MvvmCross.Platform.Droid.Platform;
using System;
using Tuba.ViewModels;

namespace Tuba.Droid.Views
{
    [Activity(Label = "Tuba Demo")]
    public class FirstView : MvxActivity<FirstViewModel>
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.FirstView);
            var recyclerView = FindViewById<MvxRecyclerView>(Resource.Id.news_list);
            if (recyclerView != null)
            {
                recyclerView.HasFixedSize = true;
                var layoutManager = new NewsLayoutManager(this);
                recyclerView.SetLayoutManager(layoutManager);
                recyclerView.AddOnScrollListener(new NewsOnScrollListener(layoutManager, () =>
                {
                    this.ViewModel.NextPageCommand.Execute();
                }));
            }
        }

        public class NewsLayoutManager : LinearLayoutManager
        {
            private NewsLinearSmoothScroller linearSmoothScroller;
            public NewsLayoutManager(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
            {

            }
            public NewsLayoutManager(Context context) : base(context)
            {
                linearSmoothScroller = new NewsLinearSmoothScroller(context);
            }
            public override void SmoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position)
            {
                linearSmoothScroller.TargetPosition = position;
                StartSmoothScroll(linearSmoothScroller);
            }
        }

        public class NewsOnScrollListener : RecyclerView.OnScrollListener
        {
            private LinearLayoutManager layoutManager;
            private Action nextPageCallback;
            private int currentItemCount;
            private bool paused;
            public NewsOnScrollListener(LinearLayoutManager layoutManager, Action nextPageCallback)
            {
                this.layoutManager = layoutManager;
                this.nextPageCallback = nextPageCallback;
            }

            public override void OnScrolled(RecyclerView recyclerView, int dx, int dy)
            {
                base.OnScrolled(recyclerView, dx, dy);

                var itemCount = recyclerView.GetAdapter().ItemCount;

                if (currentItemCount == 0)
                    currentItemCount = itemCount;

                if (itemCount != currentItemCount)
                    paused = false;

                if ((recyclerView.ChildCount + layoutManager.FindFirstVisibleItemPosition()) >= itemCount)
                {
                    if (!paused)
                    {
                        this.nextPageCallback();
                        paused = true;
                    }
                }
            }
        }

        public class NewsLinearSmoothScroller : LinearSmoothScroller
        {
            protected NewsLinearSmoothScroller(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
            {

            }

            public NewsLinearSmoothScroller(Context context) : base(context)
            {

            }
            protected override float CalculateSpeedPerPixel(DisplayMetrics displayMetrics)
            {
                return 15.0f;
            }
        }
    }
}
