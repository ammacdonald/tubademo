using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Platform.Converters;
using System.Globalization;

namespace Tuba.Droid.Converters
{
    public class TimeSinceValueConverter : MvxValueConverter<DateTime, string>
    {
        protected override string Convert(DateTime value, Type targetType, object parameter, CultureInfo culture)
        {
            var unit = string.Empty;
            var quantity = 0;
            var timeSincePosted = DateTime.Now - value;
            return string.Format("{0}m", Math.Round(timeSincePosted.TotalMinutes, MidpointRounding.ToEven));
        }
    }

}