using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using MvvmCross.Binding.Droid.Views;
using Square.Picasso;

namespace Tuba.Droid.Controls
{
    public class MockAvatarImageView : MvxImageView
    {
        public MockAvatarImageView(Context context) : base(context)
        {
            this.InitializeImage();
        }

        public MockAvatarImageView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            this.InitializeImage();
        }

        public MockAvatarImageView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
            this.InitializeImage();

        }

        protected MockAvatarImageView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
            this.InitializeImage();
        }

        private void InitializeImage()
        {
            Random random = new Random();
            var randomAvatarIndex = random.Next(0, 3);
            this.SetScaleType(ScaleType.FitCenter);
            this.SetAdjustViewBounds(true);
            int avatarId = 0;
            if (randomAvatarIndex == 0)
                avatarId = Resource.Drawable.avatar1;
            else if (randomAvatarIndex == 1)
                avatarId = Resource.Drawable.avatar2;
            else if(randomAvatarIndex == 2)
                avatarId = Resource.Drawable.avatar3;
            Picasso.With(this.Context)
               .Load(avatarId)
               .Into(this);
        }
    }
}