using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using MvvmCross.Binding.Droid.Views;
using Square.Picasso;

namespace Tuba.Droid.Controls
{
    public class MockActionIconImageView : MvxImageView
    {
        public MockActionIconImageView(Context context) : base(context)
        {
        }

        public MockActionIconImageView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public MockActionIconImageView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {

        }

        protected MockActionIconImageView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        private string action;

        public string Action
        {
            get
            {
                return action;
            }

            set
            {
                InitializeImage(value);
            }
        }

        private void InitializeImage(string action)
        {
            this.SetScaleType(ScaleType.FitCenter);
            this.SetAdjustViewBounds(true);
            int avatarId = 0;
            if (action.Equals("recognized"))
                avatarId = Resource.Drawable.fingerpointing;
            else if (action.Equals("thanked"))
                avatarId = Resource.Drawable.handshake;
            Picasso.With(this.Context)
               .Load(avatarId)
               .Into(this);
        }
    }
}