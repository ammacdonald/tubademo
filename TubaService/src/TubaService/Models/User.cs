﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TubaService.Models
{
    public class User
    {
        public string Name { get; set; }
        public string AvatarUrl { get; set; }
    }
}
