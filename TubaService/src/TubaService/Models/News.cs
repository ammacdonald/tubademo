﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TubaService.Models
{
    public class News
    {
        public int Id { get; set; }
        public DateTime PublishTime { get; set; }
        public User Actor { get; set; }
        public User Patient { get; set; }
        public string Body { get; set; }
        public List<Comment> Comments { get; set; }
        public int Shares { get; set; }
        public string Action { get; set; }
    }
}
