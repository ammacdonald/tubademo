﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TubaService.Models
{
    public class Comment
    {
        public User Poster { get; set; }
        public string Body { get; set; }
    }
}
