﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TubaService.Models;
using RandomNameGeneratorLibrary;
using NLipsum.Core;

namespace TubaService.Controllers
{
    [Route("api/[controller]")]
    public class NewsController : Controller
    {
        private static List<News> news;
        public NewsController()
        {
            if (news == null)
            {
                news = new List<News>();
                var personGenerator = new PersonNameGenerator();
                var textGenerator = new LipsumGenerator();
                var random = new Random();
                for (int i = 0; i < 50; i++)
                {
                    var paragraphs = textGenerator.GenerateParagraphs(11);
                    var comments = new List<Comment>();
                    var action = this.GetAction(random.Next(0, 2));
                    var shares = random.Next(0, 500);
                    for (int j = 1; j <= 3; j++)
                    {
                        comments.Add(new Comment() { Poster = new User() { Name = personGenerator.GenerateRandomFirstAndLastName(), AvatarUrl = "" }, Body = paragraphs[j] });
                    }
                    news.Add(new News()
                    {
                        Actor = new User() { Name = personGenerator.GenerateRandomFirstAndLastName(), AvatarUrl = "" },
                        Patient = new User() { Name = personGenerator.GenerateRandomFirstAndLastName(), AvatarUrl = "" },
                        Body = paragraphs[0],
                        PublishTime = DateTime.Now,
                        Comments = comments,
                        Id = i,
                        Action = action,
                        Shares = shares
                    });
                }
            }
        }
        private string GetAction(int index)
        {
            if (index == 0)
                return "thanked";
            else if (index == 1)
                return "recognized";
            else
                return string.Empty;
        }
        // GET api/values
        [HttpGet]
        public IEnumerable<News> Get(int page, int numberOfItems)
        {
            var itemsPerPage = news.Count / numberOfItems;
            var lastItemIndex = itemsPerPage * page + itemsPerPage;
            if (lastItemIndex <= news.Count)
            {
                var range = news.GetRange(page * itemsPerPage, itemsPerPage);
                return range;
            }
            else
                return new List<News>();
        }

        [HttpGet("{id}")]
        public News Get(string id)
        {
            return news[Convert.ToInt32(id)];
        }
    }
}
